Приложение - сервис, получающий токен авторизации и получающий доступ к работе с API

  > Авторизация на уровне приложения

  > Своя БД для каждого приложения


## Настройки приложения

Для настроек приложения нужно создать таблицу lf_app_meta.
и реализовать методы POST, GET, PATCH
В ней же можно хранить необходимые для API данные, или создать под них отдельную таблицу

Пример конфигурации

```
timezone: "Europe/Moscow" // Таймзона по умолчанию

http_home_url: https://example.com/

types: #Распределение по весам клиентов по типам аккаунтов
  0: 30
  1: 15
  2: 55

groups:
  0:
    type: 2 #vip
    geo: 3 #select countrys
  1:
    type: 2 #vip
    geo: 2 #only Ukr
  2:
    type: 2 #vip
    geo: 1 #only Rus
  3:
    type: 2 #vip
    geo: 0 #without geo
  4:
    type: 1 #pro
    geo: 3 #select countrys
  5:
    type: 1 #pro
    geo: 2 #only Ukr
  6:
    type: 1 #pro
    geo: 1 #only Rus
  7:
    type: 1 #pro
    geo: 0 #without geo
  8:
    type: 0 #min
    geo: 3 #select countrys
  9:
    type: 0 #min
    geo: 2 #only Ukr
  10:
    type: 0 #min
    geo: 1 #only Rus
  11:
    type: 0 #min
    geo: 0 #without geo
  12: #any

types_settings: 
  0: # min
    hour_limits: 20
    max_hour_limits: 50
    max_showtime: 30
    max_elements: 2
  1: # pro
    hour_limits: 40
    max_hour_limits: 150
    max_showtime: 180
    max_elements: 5
  2: # vip
    hour_limits: 160
    max_hour_limits: 501
    max_showtime: 90
    max_elements: 10
```

## Основные сущности

```
User - Пользователь. Центральная сущность
  \_Group - Группа (или task). Не может существовать отдельно от пользователя, не может существовать без страниц
    \_Page - Страница. Существует в составе группы
```
> Добавление новой страницы осуществляется или в существующую, или в новую группу

> Пользователь не может иметь более 100 страниц

## Таблицы

### lf_users_options
  
  Таблица сущности User

  >POST, GET, PATCH, DELETE

***user_id*** - POST, GET; Обязательное поле

***credits*** - POST, GET, PATCH; Средства пользователя

***workmode*** - POST, GET, PATCH; Режим работы. 
  
  >Доступны 2 значения: 
  
  0 - автоматический
  
  1 - ручной
  
  При переключении с ручного режима на автоматический, все средства с групп, которые есть у пользователя, переносятся в эту таблицу.

***type*** - POST, GET, PATCH; тип аккаунта. 
  
  >Доступны 3 значения: 0 - минимум, 1 - про, 2 - вип
  
***experience*** - GET; Значение "опыта"
  
***env*** - значение окружения, служебное поле. Не доступно из API

---

### lf_groups
  
  Таблица сущности Group входящая в состав User
  
  >POST, GET, PATCH, DELETE

  ***user_id*** - POST, GET; Обязательное поле
  
  >Возможен только тот user_id который есть в таблице users_options
  
  ***hour_limit*** - POST, GET, PATCH; ПВЧ (Показов час) 
  
  > 0 - без ограничений 
  
  ***hour_limit_current*** - GET; Текущее значение ПВЧ
  
  ***day_limit*** - POST, GET, PATCH; Суточный лимит
  
  > 0 - без ограничений
  
  ***day_limit_current*** - GET; Текущее значение суточного лимита
  
  ***geo*** - POST, GET, PATCH; Геотаргетинг
  
  > 0 - выключено, 1 - только Россия, 2 - только Украина, 3 - В этом режиме значения гео будут браться из таблицы ***showing_geo*** и должны быть записаны в неё 
  
  ***uniq_ip*** - POST, GET, PATCH; Количество часов уникальности 
  
  > 0 выключено, 168 максимальное значение
  
  ***credits*** - POST, GET, PATCH; Количество средств у группы
  
  > Может быть установлено только в ручном режиме. Устанавливаемое значение не может превышать значение в поле credits таблицы user. При установке этого значение, это же значение отнимается из поля credits таблицы user.
  
  ***interval*** - POST, GET, PATCH; Интервал между показами (ИМП)
  
  > Тип Int range [от, до]
  
  > Максимальное значение 10800. "от" меньше или равно "до". Если "от" = 0, то "до" тоже всегда 0
  
  ***name*** - POST, GET, PATCH; Название группы
  
  ***moby*** - Не доступно из API
  
  > всегда должно быть 2 при insert
  
  ***moby_ratio*** - POST, GET, PATCH; Тип устройств от 0 до 100
  
  > 0 - только ПК; 100 - только мобильные устройства
  
  ***date_add*** - GET; Таймштамп добавления группы
  
  ***date_edit*** - GET; Таймштамп последнего редактирования группы
  
  ***options*** - POST, GET, PATCH; json массив, возможные значения:

```
{
    extra: {
      debug: true, // удалять если false
      ignoreSkip: true, // удалять если false
    },
    only_proxy: true/false, // удалять если false
    cookies: [],
    blackListDomains: [],
    blackListUrls: [],
    allowUids: [],
    disallowUids: [],
    allowIps: [],
    disallowIps: [],
    device: {
      width: int,
      height: int,
      useragent: str,
      scalefactor: str,
      orient: str
    },
    cache: true/false, // удалять если false
    behavior: true/false, // удалять если false
    advmaker: true/false, // удалять если false
    bookmarks: случайное int значение между bm_from и bm_to, // удалять если 0 
    source_ratios: {
      // Суммарное значение всех value не должно превышать 100
      // Если value = 0, то enabled всегда false
      //
      // Если value = 0, enabled = false и settings.list и settings.search_engines пусты, то запись keywords удаляется
      keywords: {
        value: 30, // Значение в процентах от 0 до 100
        enabled: true/false, 
        settings: {
          list: ["Двери", "Окна", "Потолки", ...], // string array, максимум 300 значений, максимальная длина одного значения 100. Если список пустой, то значение enabled ставится в false
          search_engines: { 
            // id: вес - float с одним знаком после запятой
            // идентификаторы сейчас захардкожены и все возможные значения представлены в этом списке
            // В будущем возможные значения идентификаторов будут перемещены в таблицу.
            // Сделать GET метод возвращающий все возможные идентификаторы search_engines и их названия 
            // Если значение идентификатора - 0, то удаляется из списка
            // Если список пустой, то значение enabled ставится в false
            0: 0.8, // Google.RU
            1: 0.1, // Google.com.UA
            2: 0.1, // Google.BY
            3: 0.1, // Google.Com
            4: 0.8, // Яndex.RU
            5: 0.1, // Яndex.UA
            6: 0.1, // Яndex.BY
            7: 0.1, // Яndex.COM
            8: 0.1, // Rambler.RU
            9: 0.1, // RU.Yahoo.com
            10: 0.1, // Google.kz
            11: 0.1, // Go.Mail.RU
            13: 0.1, // Nigma.RU
            14: 0.1, // Meta.UA
            15: 0.1, // Ukr.net
            16: 0.1, // Tut.BY
            17: 0.1, // Baidu.COM
            18: 0.1, // Poisk.RU
            20: 0.1, // Bing.COM
            21: 0.1, // Aport.RU
            22: 0.1, // Yahoo.com
            23: 0.1 // Yandex.kz
          }
        }
      }, 
      // Если value = 0, enabled = false и settings пуст, то запись adsystems удаляется
      adsystems: {
        value: 20, 
        enabled: true, 
        settings: [
          // Все возможные значения представлены в этом списке
          // В будущем возможные значения будут перемещены в таблицу
          // Сделать GET метод возвращающий все возможные значения adsystems
          // Если список пустой, то значение enabled ставится в false
          "adfox", 
          "admitad", 
          "adnous", 
          "adriver", 
          "advmaker", 
          "aport", 
          "bing", 
          "B2BContext", 
          "criteo", 
          "directadvert", 
          "facebook", 
          "giraffio", 
          "adwords", 
          "adsense", 
          "instagram", 
          "kavanga", 
          "link", 
          "magna", 
          "marketgid", 
          "medialand", 
          "merchant", 
          "moimir", 
          "nnn", 
          "odnoklassniki", 
          "price", 
          "prre", 
          "rtbhouse", 
          "mytarget", 
          "taboola", 
          "tiktok", 
          "Ttarget", 
          "videonow", 
          "vkontakte", 
          "whisla", 
          "youtube"
        ]
      }, 
      // Если value = 0, enabled = false и settings.list пуст, то запись backlinks удаляется
      backlinks: {
        value: 30, 
        enabled: true, 
        settings: {
          list: ["https://google.com/", ...] // string array, максимум 300 значений, максимальная длина одного значения 2048. Если список пустой, то значение enabled ставится в false
        }
      }, 
      // Если value = 0, enabled = false и settings пуст, то запись messengers удаляется
      messengers: {
        value: 5, 
        enabled: true, 
        settings: [
          // Все возможные значения представлены в этом списке
          // В будущем возможные значения будут перемещены в таблицу
          // Сделать GET метод возвращающий все возможные значения messengers
          // Если список пустой, то значение enabled ставится в false
          "skype", 
          "telegram", 
          "viber", 
          "wechat", 
          "whatsapp", 
          "qq", 
          "discord", 
          "line", 
          "slack"
        ]
      }, 
      // Если value = 0, enabled = false, то запись clickunders удаляется
      clickunders: {
        value: 1, 
        enabled": true
      }, 
      // Если value = 0, enabled = false, то запись emailanalytics удаляется
      emailanalytics: {
        value: 4, 
        enabled: true
      }, 
      // Если value = 0, enabled = false и settings пуст, то запись socialanalytics удаляется
      socialanalytics: {
        value: 10, 
        enabled: true, 
        settings: [
          // Все возможные значения представлены в этом списке
          // В будущем возможные значения будут перемещены в таблицу
          // Сделать GET метод возвращающий все возможные значения socialanalytics
          // Если список пустой, то значение enabled ставится в false
          "facebook", 
          "youtube", 
          "instagram", 
          "tiktok", 
          "pinterest", 
          "snapchat", 
          "douyin", 
          "weibo", 
          "reddit", 
          "twitter", 
          "kuaishou", 
          "twitch", 
          "linkedin", 
          "vk", 
          "ok"
        ]
      }
    }
  }
```

  ***priority*** - POST, GET, PATCH; приоритет показа группы
  
  > От 0 до 100
  
  ***version*** - Не доступно из API
  
  > На текущий момент должно быть 2 при insert

---

### lf_groups_options

  Доп. настройки сущности Group не участвующие в выборке 

  > POST, GET, PATCH, DELETE
  
  ***group_id*** - POST, GET; Обязательное поле. 
  
  > Возможен только тот group_id который есть в таблице group_id 
  
  ***option*** - POST, GET, PATCH; Обязательное поле.
  
  ***value*** - POST, GET, PATCH; Обязательное поле.
  
  ***date*** - GET; Таймштамп добавления\редактирования записи

  ##### **Возможные значения option\value**

  1. Удерживание
  
  * option: **retention**
  * value: **true**
   
   Если false, не хранить

  2. Авторасчёт ИМП\ПВЧ каждый час
   
  * option: **autoimp**
  * value: **true**
  
  Если false, не хранить

  3. Main Time Zone - часовой пояс группы
  
  * option: **mtz**
  * value: Timezone string; Example: **Africa/Abidjan**
  
  Список поддерживаемых таймзон
  
  https://www.php.net/manual/en/timezones.php
  
  Таймзона по умолчанию берётся из app_meta. Если значение таймзоны = значению из timezone, то не хранить

  4. Автолимит (суточный)
  **Только парой**
  * option: **lm_from**
  * option: **lm_to**
  * value: **int от -500 до 500**
  
  lm_from не может быть больше чем lm_to
  
  Если обе записи 0 - они удаляются
  
  Суточное обновление полей day_limit для всех записей groups у которых есть lm_from\lm_to
  
  Суммирование случайного значения lm_from\lm_to к текущему значению day_limit 

>**Обсудить реализацию**

  Варианты:

  Полностью на стороне API

  Приложение дёргает специальный API метод, приложение само следит за частотой и временем

  Приложение самостоятельно реализует логику

  5. Переходы из закладок (суточный)
  **Только парой**

  * option: **bm_from**
  * option: **bm_to**
  * value: **int от 0 до 100**

  Если обе записи 0 - они удаляются

  При удалении **обнулять поле options.bookmarks** для записи groups

  Суточное обновление полей options.bookmarks для всех записей groups у которых есть bm_from\bm_to

  Перезапись options.bookmarks случайным значением bm_from\bm_to

>**Обсудить реализацию**

  Варианты:

  Полностью на стороне API

  Приложение дёргает специальный API метод, приложение само следит за частотой и временем

  Приложение самостоятельно реализует логику

  6. Категория
  * option: **category**
  * value: **category_id int**
  
  Возможные id представлены в таблице **lf_websites_category**, которая прямо сейчас не включена в основную БД
  
  Сделать метод возвращающий возможные категории

  7. Язык
  * option: **language**
  * value: **int**
  
  Возможные значения: 0 - русский, 1 - английский, 2 - другой
  
  Если value=0 - не хранить
  
  Сделать метод возвращающий возможные языки

---

### lf_list_countries
  Список вариантов геотаргетинга
  
  >GET

***id*** - GET; Идентификатор, ожидаемый в таблице showing_geo

***country*** - GET; ISO код страны

***region*** - GET; Название региона (если есть)

***city*** - GET; Название города (если есть)

***name*** - GET; Название страны (есть всегда)

  >Не выбирать записи с пустым полем name

### lf_pages
  Таблица сущности Page входящая в состав Group
  
  >POST, GET, PATCH, DELETE

  **group_id** - POST, GET; Обязательное поле
  
  >Возможен только тот group_id который есть в таблице groups

  **position** - POST, GET, PATCH; Позиция страницы в списке группы
  
  >от 0 до 99

  **state** - POST, GET, PATCH; Состояние страницы

  >Возможно установить только значения от 0 до 1

  0 - пауза

  1 - в работе

  При любом другом значении страница считается заблокированной. Детальное описание блокировки можно посмотреть в блэклисте.

  <s>**day_limit**</s> - не актуальное поле
  
  <s>**day_limit_current**</s> - не актуальное поле
  
  <s>**day_limit_up**</s> - не актуальное поле
  
  **showtime** - POST, GET, PATCH; Время посещения в секундах

  >Тип Int range [от, до]

   Минимальное значение 15

   Максимальное значение = app_meta.types_settings[user.type].max_showtime;

  <s>**referrers**</s> - не актуальное поле
  
  <s>**keywords**</s> - не актуальное поле
  
  **url** - POST, GET, PATCH; Обязательное поле. Список урлов

  > string array 
```
["https://example.com", ...]
```
  > Хранение в punycode, добавление протокола если нет (https)
  
  Валидация:
* Разрешённые протоколы http, https
* Не более 10 записей
* URL не может быть пусты
* не более 2048 символов в записи
* урлы в рамках страницы должны быть для одного и того же домена
* урл может быть равен about:blank (специальное значение)
* урл не в чёрном списке (про чёрный список ниже)

<s>**search_engines**</s> - не актуальное поле

**behavior** - POST, GET, PATCH; Профили посетителей

>true\false 

<s>**referrer_type**</s> - не актуальное поле

**clicks** - POST, GET, PATCH; Клики по конкретным элементам

>true\false

  **elements** - POST, GET, PATCH; Массив css селекторов

  > string array 

```
['.link', ...]
```

> Максимальное количество записей app_meta.types_settings[user.type].max_elements
Максимальная длина записи - 100

  **date_add** - GET; Таймштамп добавления страницы
  
  **date_edit** - GET; Таймштамп последнего редактирования страницы
  
  **options** - POST, GET, PATCH; json массив, возможные значения:
   

```
    {
      break_chain: от 0 до 100, // Вероятность прервать цепочку
      fixation: true/false, // Фиксация кликом. Не хранить если false; Работает только если поле elements пустое
      reading_up: true/false, // Дочитывание. Не хранить если false;
      adult: true/false, // Метка урла с контентом 18+. Не хранить если false; Проставляется автоматически согласно серому списку доменов\урлов
      neurobehavior: true/false, // Нейросетевое поведение
      actions: json массив 
        {
          blocks: [],
          triggers: [],
          anticapcha: {
            blocks: [],
            aak: ''
          }
        }, // Поведение
    }
```

---
### lf_pages_options
  
  Доп. настройки сущности Page не участвующие в выборке 
  
  >POST, GET, PATCH, DELETE
  
  **page_id** - POST, GET; Обязательное поле

  >Возможен только тот group_id который есть в таблице option - POST, GET, PATCH;
  

  **option** - POST, GET, PATCH;
  
  **value** - POST, GET, PATCH;
  
  **date** - GET; Таймштамп добавления\редактирования записи

##### **Возможные значения option\value**

  1. Планировщик
  * option: **schedules**
  * value: **json array**

   Если false, не хранить

```
[
  {
    value: pages state int, // 0 или 1
    time: timestamp int (только в будущем, таймзона - значение mtz, или значение из app_meta)
  }, ...
]
```

  > Количество записей в массиве не более 5. Только для users у которых type > 0
  
  Суточное обновление полей day_limit для всех записей groups у которых есть lm_from\lm_to для users у которых значение type > 0
  
  Суммирование случайного значения lm_from\lm_to к текущему значению day_limit
  
>**Обсудить реализацию**
  
  Варианты:
  
  Полностью на стороне API
  
  Приложение дёргает специальный API метод, приложение само следит за частотой и временем
  
  Приложение самостоятельно реализует логику

---

#### lf_pages_compiled_stats
  Таблица сводной статистики Page со срезом в 20 минут
  >GET

  ограничить по частоте запросов и количеству возвращаемых записей

  **user_id**

  **group_id**

  **page_id**

  **visits** - количество посещений

  **credits** - стоимость посещений

  **data** - json информация о настройках страницы в этот срез
 
  Пример:

```
{
  "geo": 0, 
  "url": ["https://example.com"], 
  "moby": 2, 
  "uniq_ip": 11, 
  "version": 2, 
  "behavior": true, 
  "geo_list": null, 
  "interval": [2700, 3960], 
  "showtime": [15, 30], 
  "day_limit": 123, 
  "hour_limit": 1, 
  "moby_ratio": 70, 
  "showing_list": null
}
```

  **date** - таймштамп

---

#### lf_pages_stats
  Детальная статистика Page по каждому посещению, непрерывное добавление записей 
  >GET

  ограничить по частоте запросов и количеству возвращаемых записей. Даных много, выборка тяжёлая.

  **page_id**

  **date** - таймштамп

  **data** - json информация о посещении 
  
  Пример:

  ```
  {
    "cost": 1, 
    "time": 1678993960, 
    "user": {
      "geo": 1, 
      "arch": "x64", 
      "type": "min", 
      "uuid": "win32-x64-1.6.8-062d7f97aaf1439966bd564446abe2bdce7b15053eed8eeb7432a41c7183b952-5885d7bfd654ec10a7e554e3cdd086dc", 
      "ip_addr": "188.226.122.155", 
      "user_id": 433990, 
      "version": "1.6.8"
    }, 
    "ready": 5, 
    "loaded": 9, 
    "server": "ServerName", 
    "options": {
      "userAgent": "Mozilla/5.0 (iPhone; CPU iPhone OS 14_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.1 Mobile/15E148 Safari/604.1", 
      "acceptLanguages": "ru-RU,ru,en-US,en", 
      "deviceEmulation": {
        "scale": 1, 
        "enable": true, 
        "viewSize": {
          "width": 414, 
          "height": 896
        }, 
        "screenSize": {
          "width": 1242, 
          "height": 2688
        }, 
        "orientation": "portrait", 
        "viewPosition": {
          "x": 0, 
          "y": 0
        }, 
        "deviceScaleFactor": 1
      }, 
      "windowVisibleState": false
    }, 
    "page_id": 851227, 
    "task_id": 768438, 
    "showtime": 30, 
    "workmode": 0, 
    "recipient_id": 433990, 
    "recipient_ip": "188.226.122.155", 
    "recipient_geo": 1
  }
  ```  


  **group_id**

  **user_id**
  
  **client_user_id** - id юзера, совершившего просмотр
  
  **server** - сервер юзера, совершившего просмотр
  
  **ip_addr** - ip адрес юзера, совершившего просмотр

---

#### lf_showing_geo
  Список id стран для группы у которой поле geo = 3
  POST, GET, PATCH, DELETE

  group_id - POST, GET; Обязательное поле. Возможен только тот group_id который есть в таблице 
  country - POST, GET; Обязательное поле. id из list_countries


---

#### lf_showing_times
  Часы в которые показ всех страниц для group_id будет приостановлен
  POST, GET, PATCH, DELETE
  group_id - POST, GET; Обязательное поле. Возможен только тот group_id который есть в таблице 
  hour - POST, GET; Обязательное поле. Час недели, от 1 до 168

---

#### lf_users_stats
  Не используется в API

---

#### lf_perfomance
  Не используется в API

---

#### lf_application_identifiers
  Не используется в API

---

#### lf_application_options
  Не используется в API

---

#### lf_proxy_profiles
  Не используется в API

---

## Заметки

Обращаю внимание, что в таблице реализовано каскадное удаление связанных записей
* При удалении из user_options будут удалены все связанные группы
* При удалении из groups будут удалены все связанные страницы и настройки
* При удалении из pages будут удалены все связанные настройки


Статистика сохранится даже для удалённых записей.

При удалении страницы, если в группе, к которой она принадлежала, не осталось страниц - группа должна быть удалена тоже.

---

Необходимо реализовать метод клонирования записей groups для user, включая pages и все связанные значения кроме статистики (перечислить имена таблиц), при этом соблюдая лимит в 100 записей page для user.

Обнулять поля для groups

hour_limit_current

day_limit_current

credits

date_edit

Устанавливать актуальный date_add

Обнулять поля для pages - см. клонирование записей pages

---

Необходимо реализовать метод клонирования записей pages внутри группы и все связанные значения кроме статистики (перечислить имена таблиц) и соблюдая лимит в 100 записей page для user.

Не клонировать страницы со значением state > 1

Устанавливать копии актуальный date_add

Устанавливать копии state = 0 если state = 1

Обнулять date_edit

Соблюдать position: последний position + 1

---

***Обсудить*** 

Чёрный список

Сейчас он индивидуальный для каждого приложения, что не подходит для API реализации. 

Вероятно его нужно централизовать

